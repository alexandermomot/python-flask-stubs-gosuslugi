#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import signal
import sys
from argparse import ArgumentParser
from mockservice import mock


def signal_handler(signal, frame):
    print('\nExiting by signal: ' + str(signal) + " in frame: " + str(frame))
    sys.exit(0)

p = ArgumentParser()
p.add_argument("--debug", default=False, required=False, action="store_true")
p.add_argument("--port", type=int, default=8080, required=False, action="store")
p.add_argument("--redisp", type=int, default=6379, required=False, action="store")
a = p.parse_args()

if __name__ == '__main__':

    signal.signal(signal.SIGINT, signal_handler)

    mock.run(
        host="0.0.0.0",
        port=a.port,
        debug=a.debug,
        threaded=True
    )

    signal.pause()
