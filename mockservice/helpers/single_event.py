#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import time


def event(message, sid=None, template=None, service=None, emulation=None, link_out=None, link_in=None, http_reponse=None, ctype=None):
    if sid is not None:
        sid = str(sid)
    return {
        "time": time.time(),
        "message": message,
        "endpoint": sid,
        "service": service,
        "template": template,
        "emulation": emulation,
        "link_out": link_out,
        "link_in": link_in
    }
