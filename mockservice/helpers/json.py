#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'


def request_wants_json(request):

    return request.accept_mimetypes.best_match(['application/json']) == 'application/json'
