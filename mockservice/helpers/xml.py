#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from lxml import etree
from lxml.etree import ElementBase, ETXPath


class MockXml:

    def __init__(self, rawxml):
        self.xml = etree.fromstring(rawxml)

    def find_by_xpath(self, tag, namespace):
        """
        :param element: XML root or one of the elenents from where begin search
        :param tag: Tag name to search
        :param namespace: namespace to search element
        :rtype: list[ElementBase]
        """
        return ETXPath("//{%s}%s" % (namespace, tag))(self.xml)
