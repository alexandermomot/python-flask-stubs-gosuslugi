#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import pickle


class RedisPickler:

    def __init__(self, redis_obj):
        self.redis = redis_obj

    def set(self, name, value, ex=None):
        return self.redis.set(name=name, value=pickle.dumps(value), ex=ex)

    def get(self, name):
        val = self.redis.get(name=name)
        return pickle.loads(val) if val is not None else None

    def append(self, name, value):
        ttl = self.redis.ttl(name)
        val = pickle.loads(self.redis.get(name=name))
        val.append(value)
        return self.redis.set(name, pickle.dumps(val), ttl)

    def plus(self, name):
        val = pickle.loads(self.redis.get(name=name)) + 1
        return self.redis.set(name, pickle.dumps(val))
