#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from mockservice import mock
import os
import datetime
import re


def purge(dirname, pattern):
    for f in os.listdir(dirname):
        if re.search(pattern, f):
            os.remove(os.path.join(dirname, f))


class DummyLogger:

    def __init__(self, request_headers, request_data, response_headers, response_data, who=None):

        self.filename_in  = "logs/" + datetime.datetime.now().isoformat().replace(":", "-") + "_in.log"
        self.filename_out = "logs/" + datetime.datetime.now().isoformat().replace(":", "-") + "_out.log"

        self.reqh = "".join("%s: %s\n" % (k, v) for k, v in request_headers.items()) if request_headers else ""
        self.reqd = request_data if response_headers else ""
        self.resh = "".join("%s': %s\n" % (k, v) for k, v in response_headers.items()) if response_headers else ""
        self.resd = response_data if response_data else ""

        self.who = who

    def rec(self):
        with file(os.path.join(mock.root_path, self.filename_in), "w") as f_in:
            f_in.write("====== from pgu to " + self.who + " ======\n")
            f_in.write("------ HEADERS ------\n")
            f_in.write(self.reqh)
            f_in.write("-------- DATA -------\n")
            f_in.write(self.reqd + "\n")
            f_in.flush()
            f_in.close()
        with file(os.path.join(mock.root_path, self.filename_out), "w") as f_out:
            f_out.write("====== from " + self.who + " to pgu ======\n")
            f_out.write("------ HEADERS ------\n")
            f_out.write(self.resh)
            f_out.write("-------- DATA -------\n")
            f_out.write(self.resd + "\n")
            f_out.flush()
            f_out.close()

        return {
            "log_in":  self.filename_in,
            "log_out": self.filename_out
        }
