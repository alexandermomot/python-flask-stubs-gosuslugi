#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from flask import Response, request
from werkzeug.datastructures import ImmutableMultiDict


class MockResponse(Response):

    default_mimetype = "text/xml"


def getform():
    """
    :rtype: ImmutableMultiDict
    """
    return request.form


