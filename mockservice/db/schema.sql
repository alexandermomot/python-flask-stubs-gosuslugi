drop table if exists bindings;
create table bindings (
  id integer primary key autoincrement,
  sid text not null,
  template text not null,
  endpoint text not null,
  xpath text not null,
  service text not null
);