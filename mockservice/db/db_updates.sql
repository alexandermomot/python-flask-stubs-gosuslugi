SELECT * FROM main.bindings;

ALTER TABLE bindings RENAME TO tmp_bindings;

CREATE TABLE "bindings"
(
    id INTEGER PRIMARY KEY,
    ep_mock TEXT NOT NULL,
    template TEXT NOT NULL,
    ep_proxy TEXT NULL,
    namespace TEXT NULL,
    service TEXT NOT NULL,
    type TEXT NOT NULL
);

INSERT INTO bindings(id, ep_mock, template, ep_proxy, namespace, service, type) SELECT id, ep_mock, template, ep_proxy, namespace, service, "xml" FROM tmp_bindings;

DROP TABLE main.tmp_bindings;

DELETE FROM main.bindings WHERE id = 1;

INSERT INTO main.bindings(ep_mock, template, namespace, service, type) VALUES ("services", "createOrder", "", "dpd", "xml");

UPDATE main.bindings SET ep_proxy = "http://localhost:8000" WHERE id = 0;

ALTER TABLE bindings AUTO_INCREMENT = 1;

UPDATE sqlite_sequence SET seq = 0 WHERE name = 'bingings';