#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import sqlite3, os, redis, pickle, time
from flask import Flask, g, request
from redis.exceptions import ConnectionError

mock = Flask(__name__)
# noinspection PyUnresolvedReferences
from mockservice.controllers import endpoints, index, static, rest, logs, longpool
from mockservice.helpers.redispickler import RedisPickler
from mockservice.helpers.single_event import event
from mockservice.helpers.logger import purge


@mock.before_first_request
def start():

    try:
        conn_redis = redis.StrictRedis(host="localhost", port=6379, db=0)
        conn_redis.ping()
        conn_redis.flushall()
        purge(os.path.join(mock.root_path, 'logs'), ".*log")
    except ConnectionError as conn_error:
        error = "Redis connection error: %s \nAre you sure that redis was properly installed and launched?" % conn_error.message
        raise ConnectionError(error)

    conn_redis.set("server_started", pickle.dumps(time.time()))
    conn_redis.set("last_change", pickle.dumps(time.time()))


@mock.before_request
def connects():

    g.rd = RedisPickler(redis.StrictRedis(host="localhost", port=6379, db=0))

    if not g.rd.get("last_change"):
        g.rd.set("last_change", time.time())

    if not g.rd.get("requests_served"):
        g.rd.set("requests_served", 0)

    if not g.rd.get("response_status"):
        g.rd.set("response_status", 200)

    if not g.rd.get("events"):
        g.rd.set("events", [], 7200)
        g.rd.append("events", event("Лог обновлен"))
        g.rd.append("events", event("Автоматическое выключение заглушек", None, None, None, False, None, None))

    if not g.rd.get("emulation"):
        g.rd.set("emulation", False, 3600)

    if not g.rd.get("endpoints"):  # DB either Redis info
        g.db = sqlite3.connect(os.path.normpath(os.path.abspath("mockservice/db/mock.sqlite3")))
        cur = g.db.execute("SELECT * FROM bindings")
        entries = [dict(ep_mock=row[1], template=row[2],
                        ep_proxy=row[3], namespace=row[4],
                        service=row[5], type=row[6]) for row in cur.fetchall()]
        g.rd.set("endpoints", entries, 3600)

    g.entries = g.rd.get("endpoints")

    if not isinstance(g.entries, list):
        raise Exception("Unable to load database entries for endpoints")


@mock.teardown_request
def close_db(error):
    if hasattr(g, "db"):
        db = getattr(g, "db")
        if db is not None:
            db.close()


@mock.route("/shutdown")
def shutdown():
    def shutdown_server():
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()
    shutdown_server()
    return "Server is shutting down..."


def init_db():
    """ Initializes the database. """
    connections = connects()
    with mock.open_resource('schema.sql', mode='r') as f:
        connections["db"].cursor().executescript(f.read())
    connections["db"].commit()
