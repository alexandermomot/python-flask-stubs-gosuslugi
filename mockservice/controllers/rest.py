#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from flask import Response, render_template, request
from mockservice import mock, g
from mockservice.helpers.single_event import event
from mockservice.helpers.httpauth import requires_auth
from redis import StrictRedis
import json
import time


@mock.route("/status", methods=["POST"])
def status_controls():

    # Possible cases:
    # empty POST request: shows json with status, ttl and other stuff
    # JSON with content: e.g. { "pgu": { "beta": { "emulation": true } } } - turning mocks
    #   to true ("on" state) for beta portal, else - false (transparent proxy)

    context = {}
    response = Response()
    curtime = time.time()

    context["server_started"] = curtime - g.rd.get("server_started")
    context["last_change"] = curtime - g.rd.get("last_change")
    context["requests_served"] = g.rd.get("requests_served")
    context["response_status"] = g.rd.get("response_status")

    # a = request.get_json(force=True, silent=False)
    # print "Request JSON resp statues", a["service"]["express"]["response_status"]

    if request.content_length > 0:
        incoming_json = request.get_json(force=True, silent=False)
        if incoming_json is not False and "response_status" in incoming_json["service"]["express"]:
            g.rd.set("response_status", incoming_json["service"]["express"]["response_status"])
            g.rd.append("events", event("Смена статуса ответа", None, None, None, str(g.rd.get("response_status")), None, None))
            context["response_status"] = g.rd.get("response_status")
        if incoming_json is not False and "emulation" in incoming_json["service"]["express"]:
            emulation = incoming_json["service"]["express"]["emulation"]
            if (emulation is True) or (emulation is False):
                context["emulation"] = json.dumps(emulation)
                g.rd.set("last_change", curtime)
                g.rd.set("emulation", emulation, 3600)
                if "manual" in incoming_json and incoming_json["manual"]:
                    event_type = "Переключение заглушек через веб-интерфейс"
                else:
                    event_type = "Переключение заглушек via REST"
                g.rd.append("events", event(event_type, None, None, None, emulation, None, None))
                response = Response(render_template("json/status.json", **context))
        else:
            error = "Please, set the correct value to emulation parameter: true or false"
            response = Response(render_template("json/error.json", error=error), status=400)
    else:
        context["emulation"] = json.dumps(g.rd.get("emulation"))
        response = Response(render_template("json/status.json", **context))

    response.content_type = "application/json"
    return response
