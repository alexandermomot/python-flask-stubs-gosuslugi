#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from flask import Response
from mockservice import mock
from mockservice.helpers.httpauth import requires_auth
import os


@mock.route("/logs/<file_id>", methods=["GET"])
def logs(file_id):
    fd = file(os.path.join(mock.root_path, "logs/" + file_id), "r")
    content = fd.read()
    fd.close()
    return Response(content, mimetype="text/plain")
