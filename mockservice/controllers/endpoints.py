#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from flask import render_template, request as flask_request, abort, jsonify
from mockservice import mock, g
from mockservice.helpers import DummyLogger, MockXml, MockResponse as Response
from mockservice.helpers.single_event import event
from mockservice.helpers.json import request_wants_json

from requests import request as RedirectRequest
import os


@mock.route("/<service>/<string:ep_request>", methods=["POST", "GET"])
def endpoints(service, ep_request):

    args = flask_request.args  # type: dict

    if service == "ponyexpress":
        abort(500)

    e_template = {
        "STATE_ERROR": {
            "reason": "Mocks is in unexpected state. It is turned on or turned off?",
            "ep_request": ep_request,
            "service": service
        },
        "NO_RESPONSE_REDIRECT": {
            "reason": "Cant find endpoint in database to where Request should be redirected",
            "ep_request": ep_request,
            "service": service
        },
        "NO_SAMPLE_OR_EP": {
            "reason": "Cant find endpoint or sample for response",
            "ep_request": ep_request,
            "service": service
        },
    }

    ep_mock, template, ep_proxy, content_type = None, None, None, None,
    results, row = [], []
    context, h = {}, {}

    incoming = {  # Preparing dict from incoming headers and data
        "headers": {k: v for k, v in flask_request.headers.__iter__() if k != "Host"},
        "data": flask_request.data,
    }

    emulation = g.rd.get("emulation")
    entries = g.entries

    if emulation is True:  # Mocks is ON

        if "wsdl" in args and flask_request.method == "GET":
            g.rd.append("events", event("Запрос WSDL сервиса", ep_request, None, service, emulation, None, None))
            g.rd.plus("requests_served")
            response_file = "services/%(service)s/service.wsdl" % {"service": service}
            return Response(render_template(response_file), status=200)

        try:
            incoming_data = flask_request.get_json() or MockXml(flask_request.data)
        except Exception as e:
            if mock.debug:
                xml_error = {"ep_request": ep_request, "service": service, "reason": e.__class__.__name__}
                resp = Response(render_template("services/404.xml", **xml_error))
                f = DummyLogger(incoming["headers"], incoming["data"], resp.headers, resp.data, "mock").rec()
                g.rd.append("events",
                            event("Запрос содержит синтаксически неверные данные", ep_request, None, service,
                                  emulation, f["log_in"], f["log_out"]))
                g.rd.plus("requests_served")
                return resp
            else:
                raise e



        if service == "pick-point":
            for row in entries:
                if row["ep_mock"] == ep_request and row["template"] in incoming_data:
                    content_type = row['type']
                    template = row["template"]
                    results = True

        if service == "express":
            for row in entries:
                if row["ep_mock"] == "opsapi":
                    results = incoming_data.find_by_xpath(row["template"], row["namespace"])
                    content_type = row['type']
                    if results:
                        ep_mock, template = row["ep_mock"], row["template"]
                        break

        if service == "dpd":
            for row in entries:
                if row["ep_mock"] == "services":
                    results = incoming_data.find_by_xpath(row["template"], row["namespace"])
                    content_type = row['type']
                    if results:
                        ep_mock, template = row["ep_mock"], row["template"]
                        break

        replace_temp = {'service': service, 'ep_request': ep_request, 'template': template, 'type': content_type}
        response_file = "services/%(service)s/%(ep_request)s/%(template)s/response.%(type)s" % replace_temp
        g.rd.plus("requests_served")

        response_status = g.rd.get("response_status")

        if response_status == 500:
            f = DummyLogger(incoming["headers"], incoming["data"], None, None, "mock").rec()
            g.rd.append("events", event("Возвращаю " + str(response_status), ep_request,
                                        template, service, emulation, f["log_in"], f["log_out"]))
            abort(500)

        if not results or not os.path.isfile(os.getcwd() + "/mockservice/templates/" + response_file):
            resp = Response(render_template("services/404.xml", **e_template["NO_SAMPLE_OR_EP"]), status=404)
            f = DummyLogger(incoming["headers"], incoming["data"], resp.headers, resp.data, "mock").rec()
            g.rd.append("events", event("Образец не найден", ep_request, None, service, emulation, f["log_in"], f["log_out"]))
        else:
            resp = Response(render_template(response_file, **context), status=200)
            f = DummyLogger(incoming["headers"], incoming["data"], resp.headers, resp.data, "mock").rec()
            g.rd.append("events", event("Образец найден", ep_request, template, service, emulation, f["log_in"], f["log_out"]))
        return resp

    elif emulation is False:  # Mocks is OFF
        redirect_url = None
        for rec in entries:
            if rec["service"] == service and rec["ep_mock"] == ep_request:
                redirect_url = rec["endpoint"]
                break
        # PguRequest -> flask_request -> RedirectRequest -> dept_response -> Response -> pgu_response
        if redirect_url:
            dept_response = RedirectRequest("POST", url=redirect_url, headers=incoming["headers"], data=incoming["data"])
            pgu_response = Response(response=dept_response.content, status=200)
            [pgu_response.headers.set(k, v) for k, v in dept_response.headers.iteritems()]
            f = DummyLogger(incoming["headers"], incoming["data"], pgu_response.headers, pgu_response.data, "department").rec()
            g.rd.append("events", event("Проксирование успешно", ep_request, None, service, emulation, f["log_in"], f["log_out"]))
            g.rd.plus("requests_served")
            return pgu_response

        else:
            resp = Response(render_template("services/404.xml", **e_template["NO_RESPONSE_REDIRECT"]), status=400)
            f = DummyLogger(incoming["headers"], incoming["data"], resp.headers, resp.data, "department").rec()
            g.rd.append("events", event("Проксирование не удалось. Не найден endpoint в БД", ep_request, None, service, emulation, f["log_in"], f["log_out"]))
            g.rd.plus("requests_served")
            return resp

    else:
        g.rd.append("events", event("Неизвестное состояние заглушки", ep_request, None, service, None, None, None))
        g.rd.plus("requests_served")
        return Response(render_template("services/404.xml", **e_template["STATE_ERROR"]), status=400)
