#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import os
from mockservice import mock
from flask import send_from_directory


@mock.route("/scripts/<string:javascript>.js")
def informers(javascript):
    return send_from_directory(os.path.join(mock.root_path, 'templates/scripts'), javascript + ".js")


@mock.route("/styles/<string:style>.css")
def styles(style):
    return send_from_directory(os.path.join(mock.root_path, 'templates/styles'), style + ".css")


@mock.route("/images/<string:image>.<string:itype>")
def images(image, itype):
    return send_from_directory(os.path.join(mock.root_path, 'templates/images'), "%s.%s" % (image, itype))
