#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from flask import render_template, Response, send_from_directory
from mockservice import mock, g
from mockservice.helpers.httpauth import requires_auth
import os


@mock.route("/favicon.ico")
def favicon():
    return send_from_directory(os.path.join(mock.root_path, 'templates'), 'favicon.ico')


@mock.route("/", methods=["GET"])
def index():

    response = Response(render_template("index.html", rows=g.rd.get("endpoints")), status=200)
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "0"

    return response
