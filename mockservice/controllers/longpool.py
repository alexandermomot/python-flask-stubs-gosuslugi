#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from flask import Response, request
from mockservice import mock, g
from redis import StrictRedis
import json
import time


@mock.route("/poll/events", methods=["POST"])
def log_events_polling():  # TODO: real subsription to the redis events

    start_time = time.time()
    last_response_time = request.args.get("last")
    refresh = 1
    timeout = 30

    if last_response_time is None:
        response = Response(json.dumps(g.rd.get("events")), mimetype="application/json", status=200)
    else:
        end_time = start_time + timeout  # server-side long poll interval
        last_response_time = float(last_response_time)
        while end_time > time.time():
            event_list = g.rd.get("events")
            last_event_time = max(event_list, key=lambda x: x["time"])["time"]
            if last_event_time > last_response_time:
                events = [event for event in event_list if event["time"] > last_response_time]
                response = Response(json.dumps(events), mimetype="application/json", status=200)
                return response
            else:
                time.sleep(refresh)  # internal pool: n sec interval between requests to redis
        response = Response(json.dumps({}), mimetype="application/json", status=200)
    return response


# @mock.route("/push/status", methods=["GET"])
# def status_push():
#     # TODO: real subsription to the redis events
#     # TODO: не работает с uwsgi, разобраться почему
#
#     from mockservice.helpers.sse import ServerSentEvent
#     def status(rd_link):
#         redis = rd_link  # type: StrictRedis
#         previous = redis.get("emulation")
#         try:
#             while True:
#                 time.sleep(1)  # internal pool: 1 sec interval between requests to redis
#                 current_status = redis.get("emulation")
#                 if previous != current_status:
#                     previous = current_status
#                     yield ServerSentEvent(json.dumps(current_status)).encode()  # returns every time when status changes
#         except GeneratorExit:
#             return
#
#     return Response(status(g.rd), mimetype="text/event-stream", status=200)