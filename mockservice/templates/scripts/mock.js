
var status_url = "/status";
var eventpool_url = "/poll/events";


function dateCalc(timearg) {
    var timearr = timearg.toString().split(".");
    var fullPart = parseInt(timearr[0]*1000);
    var date = new Date(fullPart);
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var milliseconds = parseInt(timearr[1].slice(0, 4));
    return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + '.' + milliseconds;
}


function nn(value) {
    return (value == null) ? "" : value
}

function getStatus() {
    $.ajax({
        method: "POST",
        url: status_url,
        cache: false,
        data: null,
        success: function(data) {
            $("#cmn-toggle-1").prop("checked", data["service"]["express"]["emulation"]);
            $("#uptime").text(new Date(new Date() - parseInt(data["server_uptime"].toString().split(".")[0]) * 1000));
            $("#requests-served").text(data["requests_served"])
        },
        error: function(){
            $("#informer-alert").slideToggle();
        }
    })
}

function setActions() {
    $("#show-bindings").click(function() {
        $("#bindings").toggle();
    });

    $("table#events").bind("DOMSubtreeModified", function() {
        $(".events").fadeTo(200, 1);
    });

    $('a#cln-log').click(function(){
        $(".events").fadeTo(200, 0, function(){
            $("#event-log").empty();
            $(".events").fadeTo(200, 1);
        });
    });

    $('a[name="refresh-button"]').click(function(e) {
        location.reload(true);
    });

    $('#cmn-toggle-1').click(function(e) {
        $.ajax({
            method: "POST",
            url: status_url,
            cache: false,
            data: JSON.stringify({"manual": true, "service": {"express": {"emulation": this.checked}}})
        })
    });

    $('#cmn-toggle-2').click(function(e) {
        responseStatus = !this.checked ? 200 : 500
        $.ajax({
            method: "POST",
            url: status_url,
            cache: false,
            data: JSON.stringify({"manual": true, "service": {"express": {"response_status": responseStatus, "emulation": this.checked}}})
        })
    });

}


function poll(poolTimeQuery){
    poolTimeQuery = (poolTimeQuery == undefined) ? 0 : poolTimeQuery;
    var lastEvent = {};
    var timeOfQuery = null;
    var EventsServerError = false;
    var request = $.ajax({
        url: eventpool_url + "?last=" + poolTimeQuery,
        method: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 60000,
        data: null,
        success: function(data) {
            if (!$.isEmptyObject(data)) {
                data.forEach(function(event) {
                    var eventState = ''; var servEp = '';
                    if (event.emulation) { eventState = ' success'; }
                    if (event.emulation && event.template == null) { eventState = ' danger'; }
                    if (event.link_in == null && event.link_out == null && event.template == null) { eventState = ' info'; }
                    if (event.service != null && event.endpoint != null) { servEp = nn(event.service) + " &frasl; " + nn(event.endpoint) }
                    var row = $(
                    '<tr class="events'+eventState+'" style="opacity: 0.0;">'+
                        '<td>' + dateCalc(event.time) + '</td>'+
                        '<td>' + servEp + '</td>'+
                        '<td>' + nn(event.template) + '</td>'+
                        '<td>' + nn(event.message) + '</td>'+
                        '<td><a href="' + nn(event.link_out) + '">' + nn(event.link_out) + '</td>'+
                        '<td><a href="' + nn(event.link_in) + '">' + nn(event.link_in) + '</td>'+
                        '<td>' + nn(event.emulation) + '</td>'+
                        '<td>' + nn(null) + '</td>' +
                    '</tr>');
                    $("table#events > tbody:first").prepend(row);
                });
                lastEvent = data[data.length-1];
                $("#cmn-toggle-1").prop("checked", lastEvent.emulation); }
            timeOfQuery = ("time" in lastEvent && lastEvent.time != null) ? lastEvent.time : poolTimeQuery;
        },
        error: function(){
            EventsServerError = true;
            $("#informer-alert").slideToggle();
        },
        complete: function(){
            if (EventsServerError == true) {
                request.abort();
            } else {
                poll(timeOfQuery);
            }
        }
    });
}


$(document).ready(function(){
    getStatus();
    setActions();
    poll();
    setInterval(getStatus, 60000);
});


